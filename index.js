// bai tap 1

/* 

* input: số ngày đi làm

* step: lương 1 ngày * số ngày đi làm

* output: số tiền lương

*/

var so_ngay = 60;

var luong_mot_ngay = 100000;

var luong;

luong = so_ngay * luong_mot_ngay;

console.log("luong: ", luong);

// bai tap 2

/**
 * input: nhập 5 số
 * step: (tổng 5 số)/5
 * output: giá trị trung bình
 */

var num1 = 1,
  num2 = 2,
  num3 = 3,
  num4 = 4,
  num5 = 5;

var gia_tri_trung_binh;

gia_tri_trung_binh = (num1 + num2 + num3 + num4 + num5) / 5;

console.log("gia_tri_trung_binh: ", gia_tri_trung_binh);

// bai tap 3

/**
 * input: số tiền usd
 * step: số tiền usd*23.500
 * output: vnd
 */

var usd = 10;

var vnd;

vnd = usd * 23500;

console.log("vnd: ", vnd);

// bai tap 4
/**
 * input: chiều dài và chiều rộng
 * step:
 *      s1: S = dài * rộng
 *      s2: C = (dài + rộng)*2
 * output: dien tich va chu vi
 */

var dai = 20,
  rong = 10;

var S, C;

S = dai * rong;

console.log("dien tich: ", S);

C = (dai + rong) * 2;

console.log("chu vi: ", C);

// bai tap 5

/**
 * input : nhập số bất kì 2 chữ số ( đặt là num)
 * step:
 *      s1 : tìm số hàng đơn vị = num chia 10 lấy phần dư
 *      s2 : tim so hang chuc = num chia cho 10 và làm tròn xuống( mục đích để lấy phần nguyên)
 *      s3 : cộng hai số hàng chục và đơn vị
 * output: tổng 2 ký số
 */

var num = 15;

var hang_dv, hang_chuc, tong;

hang_dv = num % 10;

hang_chuc = num / 10;

tong = Math.floor(hang_chuc) + Math.floor(hang_dv);

console.log("tong: ", tong);
